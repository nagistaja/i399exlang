'use strict';

var a = 0;
var b = 0;
var c = 0;
var d = 0;

console.log(a);

add(1, 1, function (sum1) {
    console.log(b);
    add(sum1, 1, function (sum2) {
        console.log(sum2);
    });
    console.log(c);
});

console.log(d);

function add(x, y, callback) {
    callback(x + y);
}
