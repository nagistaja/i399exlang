'use strict';

var array = [1, 2, 3, 4, 5, 6];

var oddNumbers = myFilter(array, function (each) {
    return each % 2 === 1;
});

console.log('Odd numbers: ' + oddNumbers);

function myFilter(list, predicate) {
    // filter out and return elements that
    // satisfy the predicate function
}